/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2doexamenestructura;

/**
 *
 * @author Kevin Angel Torrez Vedia     8514889pt
 */
public class EstrucCola<T>{
    private Nodocola<T> primero;
    private Nodocola<T> ultimo;
    private int tamanio;
    private int limite;
    
    public EstrucCola(){
        this.primero = null;
        this.ultimo = null;
        tamanio = 0;
    }
    
    public boolean estaVacio(){
        return primero == null; 
    }
    
    public int tamanioCola(){
        return tamanio;
    }
    
    public T primerelemento(){
        if(estaVacio()){
            return null;
        }
        return primero.getElemento();
    }
    
    public void introducirDato(T elemento){
        Nodocola<T> nuevo = new Nodocola(elemento,null);
        if(estaVacio()){
            primero = nuevo;
            ultimo = nuevo;
        }else{
            if(tamanioCola() == 1){
                primero.setSiguiente(nuevo);
            }else{
                ultimo.setSiguiente(nuevo);
            }
            ultimo = nuevo;
        }
        tamanio++;
    }
    
    public T sacarDato(){
        if(estaVacio()){
            return null;
        }
        
        T elemento = primero.getElemento();
        Nodocola<T> aux = primero.getSiguiente();
        primero = aux;
        tamanio--;
        if(estaVacio()){
            ultimo = null;
        }
        return elemento;
    }
    public void setLimite(int limite){
        this.limite=limite;
    }
    public Boolean estaAlLimite(){
        return tamanio==limite;
    }
}
