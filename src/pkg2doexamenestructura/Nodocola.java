/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2doexamenestructura;

/**
 *
 * @author Kevin Angel Torrez Vedia     8514889pt
 */
public class Nodocola <T>{
    private T elemento;
    private Nodocola<T> siguiente;
    
    public Nodocola(T elemento, Nodocola<T> siguiente){
        this.elemento = elemento;
        this.siguiente = siguiente;
    }

    public T getElemento() {
        return elemento;
    }

    public void setElemento(T elemento) {
        this.elemento = elemento;
    }

    public Nodocola<T> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodocola<T> siguiente) {
        this.siguiente = siguiente;
    }

}
